import React, {Component} from 'react';
import CardDeck from './components/CardDeck/CardDeck'
import './App.css';
import './components/Card/Card.css'
import Card from "./components/Card/Card";
import PokerHand from "./components/PokerHand/PokerHand";

class App extends Component {

    state = {
        cards: [],
        value: '',
    };

    getCardsHandler = () => {
        let cards = new CardDeck().getCards(5);

        let value = new PokerHand().getOutcome(cards)
        this.setState({cards: cards, value: value[0]});

    };

    render() {
        return (
            <div className="App">
                <div className="card-block">
                    <div>
                        <button onClick={this.getCardsHandler}>Show cards</button>

                    </div>
                    <Card card={this.state.cards} value={'Results: ' + this.state.value}/>
                </div>

            </div>
        );
    }
}

export default App;