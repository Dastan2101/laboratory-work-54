import React from 'react';
import './Card.css'

const Card = (props) => {

    return (
        <div className='cards'>
            <p> {props.value}</p>

            {props.card.map((card, key) => {
                return (
                    <div key={key} className={`Card Card-rank-${card.rank} Card-${card.suit}`}>
                        <span className="Card-rank">{card.rank}</span>
                        <span className="Card-suit">{card.mark}</span>
                    </div>
                )
            })}

        </div>
    );


};

export default Card;