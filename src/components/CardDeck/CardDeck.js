class CardDeck {
    constructor() {
        this.array = [];

        this.cards = [];
        const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
        const suit = ['hearts', 'diams', 'spades', 'clubs'];
        const mark = ['♥', '♦', '♠', '♣'];

        for (let i = 0; i < ranks.length; i++) {
            for (let j = 0; j < suit.length; j++) {
                let object = {
                    rank: ranks[i],
                    suit: suit[j],
                    mark: mark[j]
                };

                this.cards.push(object);
            }
        }
    };

    getCard() {
        let indexOfArray = Math.floor(Math.random() * this.cards.length);
        let newCard = this.cards[indexOfArray];
        this.cards.splice(indexOfArray, 1);
        return newCard;
    };

    getCards(howMany) {
        let cardes = [];

        while (cardes.length < howMany) {
            let card = this.getCard();
            cardes.push(card);
        }
        return cardes;
    }
}

export default CardDeck;





