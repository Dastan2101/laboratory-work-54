class PokerHand {

    getOutcome = (array) => {
        let resultsArray = [];
        let count = 0;
        let flush = 0;

        for (let i = 0; i < array.length; i++) {

            for (let j = i + 1; j < array.length; j++) {
                if (array[i].suit === array[j].suit) {
                    flush++;
                    if (flush === 7) {
                        resultsArray = [];

                        resultsArray.push('Flush')
                    }

                }
                if (array[i].rank === array[j].rank) {

                    count++;

                    if (count === 5) {

                        resultsArray = [];
                        resultsArray.push('Four of a kind')
                    } else if (count === 3) {
                        resultsArray = [];

                        resultsArray.push('Three of a kind')
                    } else if (count === 2) {
                        resultsArray = [];

                        resultsArray.push('Two pairs')


                    } else if (count === 1) {
                        resultsArray = [];
                        resultsArray.push('One pair')

                    }else if (count === 4) {
                        resultsArray = [];
                        resultsArray.push('Full House')
                    }

                }

                else if (count === 0) {
                    resultsArray = [];
                    resultsArray.push('No good combination')
                }

            }
        }


        return resultsArray;

    }

}

export default PokerHand;